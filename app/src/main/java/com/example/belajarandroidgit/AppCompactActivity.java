package com.example.belajarandroidgit;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public abstract class AppCompactActivity extends AppCompatActivity {
    /*    @Override
        protected abstract void onCreate(Bundle savedInstanceState);*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
